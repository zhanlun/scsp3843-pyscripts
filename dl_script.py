import urllib.request
import shutil
import os

urls = [
    "http://pages.cs.wisc.edu/~anhai/data/784_data/movies1/csv_files/rotten_tomatoes.csv",
    "http://pages.cs.wisc.edu/~anhai/data/784_data/movies1/csv_files/imdb.csv",
    "http://pages.cs.wisc.edu/~anhai/data/784_data/movies5/csv_files/roger_ebert.csv",
    "http://pages.cs.wisc.edu/~anhai/data/784_data/movies5/csv_files/imdb.csv"
]

target_dir = 'dataSource/'

def download():
    os.mkdir(target_dir)
    for url in urls:
        filename = url.split('/')[-1]
        with urllib.request.urlopen(url) as response:
            fullpath = target_dir + filename
            while os.path.isfile(fullpath):
                fullpath = target_dir + '2-' + filename
                filename = '2-' + filename
            with open(fullpath, 'wb') as out_file:
                shutil.copyfileobj(response, out_file)

if __name__ == '__main__':
    download()